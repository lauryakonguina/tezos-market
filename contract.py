import smartpy as sp

class Prediction(sp.Contract):
    def __init__(self, harbinger, admin):
        self.init(
            admin = admin,
            harbinger = harbinger,
            paused = False,
            soft_pause = False,
            hard_pause = False,
            assets = {"XTZ": "XTZ-USD", "BTC": "BTC-USD", "ETH": "ETH-USD", "LINK": "LINK-USD", "COMP": "COMP-USD"},
            markets_id = 0,
            markets = sp.big_map(
                tvalue = sp.TRecord(
                    user = sp.TAddress,
                    asset = sp.TString,
                    type = sp.TString,
                    price = sp.TNat,
                    status = sp.TString,
                    true = sp.TMutez,
                    false = sp.TMutez,
                    time = sp.TTimestamp,
                    odd = sp.TNat,
                    winner = sp.TString
                )
            ),
            bets_id = 0,
            bets = sp.big_map(
                tvalue = sp.TRecord(
                    user = sp.TAddress,
                    market_id = sp.TNat,
                    bet = sp.TString,
                    amount = sp.TMutez,
                    redeem = sp.TBool
                )
            ),
            resolve = 0
        )
    
    @sp.entry_point
    def set_admin(self, params):
        sp.verify(sp.sender == self.data.admin, "Not admin")
        self.data.admin = params.admin

    @sp.entry_point
    def set_contract(self, params):
        sp.verify(sp.sender == self.data.admin, "Not admin")
        self.data.harbinger = params.harbinger

    @sp.entry_point
    def set_asset(self,params):
        sp.verify(sp.sender == self.data.admin, "Not admin")
        self.data.assets[params.currency] = params.asset

    @sp.entry_point
    def del_asset(self, params):
        sp.verify(sp.sender == self.data.admin, "Not admin")
        del self.data.assets[params.currency]

    @sp.entry_point
    def set_market(self, params):
        sp.set_type(params, sp.TRecord(asset = sp.TString, type = sp.TString, price = sp.TNat, time = sp.TTimestamp))
        sp.verify(
            self.data.assets.contains(params.asset) &
            ((params.type == "inferior") | (params.type == "superior")),
            "Asset not available"
        )
        self.data.markets_id += 1
        self.data.markets[self.data.markets_id] = sp.record(
            user = sp.sender,
            asset = params.asset,
            type = params.type,
            price = params.price,
            status = "ongoing",
            true = sp.mutez(0),
            false = sp.mutez(0),
            time = params.time,
            odd = 0,
            winner = ""
        )
    
    @sp.entry_point
    def set_bet(self, params):
        sp.set_type(params, sp.TRecord(market_id = sp.TNat, bet = sp.TString))
        sp.verify(
            self.data.markets.contains(params.market_id) &
            (self.data.markets[params.market_id].status == "ongoing") &
            ((params.bet == "true") | (params.bet == "false")) &
            (sp.amount >= sp.mutez(2000000)),
            "Betting parameters are incorrect"
        )
        self.data.bets_id += 1
        self.data.bets[self.data.bets_id] = sp.record(
            user = sp.sender,
            market_id = params.market_id,
            bet = params.bet,
            amount = sp.amount,
            redeem = False
        )
        sp.if params.bet == "true":
            self.data.markets[params.market_id].true += sp.amount
        sp.else:
             self.data.markets[params.market_id].false += sp.amount
    
    @sp.entry_point
    def del_bet(self, params):
        sp.set_type(params, sp.TRecord(bet_id = sp.TNat))
        sp.verify(
            self.data.bets[params.bet_id].user == sp.sender,
            "Cannot cancel your bet"
        )
    
    @sp.entry_point
    def resolve(self, params):
        sp.set_type(params, sp.TRecord(market_id = sp.TNat))
        sp.verify(
            self.data.markets.contains(params.market_id) &
            (sp.now >=self.data.markets[params.market_id].time),
            "You're market can not be resolved"
        )

        asset = self.data.assets[self.data.markets[params.market_id].asset]
        handle = sp.contract(
            sp.TPair(sp.TString, sp.TPair(sp.TTimestamp, sp.TNat)),
            sp.self_address,
            entry_point = "callback"
        ).open_some()
        param = (asset, handle)

        contract = sp.contract(sp.TPair(sp.TString, sp.TContract(sp.TPair(sp.TString, sp.TPair(sp.TTimestamp, sp.TNat)))), self.data.harbinger, entry_point="get").open_some()

        self.data.resolve = params.market_id
        sp.transfer(param, sp.mutez(0), contract)

    @sp.entry_point
    def callback(self, requestPair):
        sp.set_type(requestPair, sp.TPair(sp.TString, sp.TPair(sp.TTimestamp, sp.TNat)))
        result = sp.compute(sp.snd(sp.snd(requestPair)))

        market_id = self.data.resolve
        price = self.data.markets[market_id].price
        type = self.data.markets[market_id].type

        true = self.data.markets[market_id].true
        false = self.data.markets[market_id].false

        sp.if (false >= sp.mutez(2000000)) & (true >= sp.mutez(2000000)):
            true = sp.utils.mutez_to_nat(true)
            false = sp.utils.mutez_to_nat(false)
            sp.if type == "inferior":
                sp.if result < price:
                    self.data.markets[market_id].winner = "true"
                    self.data.markets[market_id].odd = ((false+true)*100/true)
                sp.else:
                    sp.if result > price:
                        self.data.markets[market_id].winner = "false"
                        self.data.markets[market_id].odd = ((false+true)*100/false)
                    sp.else:
                        self.data.markets[market_id].winner = "equal"
                        self.data.markets[market_id].odd = 100
            sp.else:
                sp.if result > price:
                    self.data.markets[market_id].winner = "true"
                    self.data.markets[market_id].odd = ((false+true)*100/true)
                sp.else:
                    sp.if result > price:
                        self.data.markets[market_id].winner = "false"
                        self.data.markets[market_id].odd = ((false+true)*100/false)
                    sp.else:
                        self.data.markets[market_id].winner = "equal"
                        self.data.markets[market_id].odd = 100
        sp.else:
            self.data.markets[market_id].winner = "canceled"
            self.data.markets[market_id].odd = 100
        self.data.markets[market_id].status = "resolved"

    @sp.entry_point
    def redeem(self, params):
        sp.set_type(params, sp.TRecord(bet_id = sp.TNat))
        market_id = self.data.bets[params.bet_id].market_id
        sp.verify(
            self.data.bets.contains(params.bet_id) &
            (self.data.bets[params.bet_id].redeem == False) &
            (sp.sender == self.data.bets[params.bet_id].user) &
            (self.data.markets[market_id].status == "resolved") &
            (
                (self.data.markets[market_id].winner == self.data.bets[params.bet_id].bet) |
                (self.data.markets[market_id].winner == "canceled") |
                (self.data.markets[market_id]. winner == "equal")
            ),
            "You're bet is not redeemable"
        )

        res = sp.utils.mutez_to_nat(self.data.bets[params.bet_id].amount)
        amount = sp.utils.nat_to_mutez((res*self.data.markets[market_id].odd)/100)
        sp.send(sp.sender, amount, message = None)
        self.data.bets[params.bet_id].redeem = True

sp.add_compilation_target(
    "step",
    Prediction(
        admin = sp.address('ADMIN_ADDRESS'), 
        harbinger = sp.address('HARBINHER_ADDRESS')
    )
)
